[[_TOC_]]

# Summary

Very simple nonsense distroless build of BusyBox. 

# Who is this for? 

Anyone that wants a simple 1 layer build. Busybox comes from Arch Linux and goes in an empty container. It's ONLY 
BusyBox and nothing else. If you want a blank container with a basic shell, here it is.
